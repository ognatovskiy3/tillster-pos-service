package com.tillster.posservice.api.model.result;

import com.fasterxml.jackson.annotation.JsonRootName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonRootName(value = "posLoyaltySubmitOrderResult")
public class PosLoyaltySubmitOrderResult {
    private String loyaltyOrderID;
    private long loyaltyPointsBalance;
    private long loyaltyPointsEarned;
    private long loyaltyPointsRedeemed;
    private String promoMessage;
    private String promoImage;
    private boolean accrualLimitReached;
    private Integer currentAccrualLimit;
    private String resultDescription;
    private int statusCode;

    public String getLoyaltyOrderID() {
        return loyaltyOrderID;
    }

    public void setLoyaltyOrderID(String loyaltyOrderID) {
        this.loyaltyOrderID = loyaltyOrderID;
    }

    public long getLoyaltyPointsBalance() {
        return loyaltyPointsBalance;
    }

    public void setLoyaltyPointsBalance(long loyaltyPointsBalance) {
        this.loyaltyPointsBalance = loyaltyPointsBalance;
    }

    public String getPromoImage() {
        return promoImage;
    }

    public void setPromoImage(String promoImage) {
        this.promoImage = promoImage;
    }

    public String getPromoMessage() {
        return promoMessage;
    }

    public void setPromoMessage(String promoMessage) {
        this.promoMessage = promoMessage;
    }

    public long getLoyaltyPointsEarned() {
        return loyaltyPointsEarned;
    }

    public void setLoyaltyPointsEarned(long loyaltyPointsEarned) {
        this.loyaltyPointsEarned = loyaltyPointsEarned;
    }

    public long getLoyaltyPointsRedeemed() {
        return loyaltyPointsRedeemed;
    }

    public int getCurrentAccrualLimit() {
        return currentAccrualLimit;
    }

    public void setLoyaltyPointsRedeemed(Long loyaltyPointsRedeemed) {
        this.loyaltyPointsRedeemed = loyaltyPointsRedeemed;
    }

    public void setAccrualLimitReached(boolean accrualLimitReached) {
        this.accrualLimitReached = accrualLimitReached;
    }

    public boolean isAccrualLimitReached() {
        return accrualLimitReached;
    }

    public void setCurrentAccrualLimit(Integer currentAccrualLimit) {
        this.currentAccrualLimit = currentAccrualLimit;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
