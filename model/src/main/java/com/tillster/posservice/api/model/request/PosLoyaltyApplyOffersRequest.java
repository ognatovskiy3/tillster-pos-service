package com.tillster.posservice.api.model.request;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.tillster.posservice.api.model.PosHeader;
import com.tillster.posservice.api.model.PosLoyaltyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonRootName(value = "posLoyaltyApplyOffersRequest")
public class PosLoyaltyApplyOffersRequest {
    private String shortCode;
    private PosHeader header;
    private PosLoyaltyOrder order;

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public PosHeader getHeader() {
        return header;
    }

    public void setHeader(PosHeader header) {
        this.header = header;
    }

    public PosLoyaltyOrder getOrder() {
        return order;
    }

    public void setOrder(PosLoyaltyOrder order) {
        this.order = order;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
