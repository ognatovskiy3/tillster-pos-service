package com.tillster.posservice.api.model;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PosLoyaltyDiscount {
    private String id;
    private PosLoyaltyDiscountType type;
    private BigDecimal price;
    private String description;
    private String reference;
    private String discountCode;
    private Short loyaltyCost;

    PosLoyaltyDiscount() {}

    PosLoyaltyDiscount(PosLoyaltyDiscountType type, String id, BigDecimal price, String description, String reference, String discountCode, Short loyaltyCost) {
        this.type = type;
        this.id = id;
        this.price = price;
        this.description = description;
        this.reference = reference;
        this.discountCode = discountCode;
        this.loyaltyCost = loyaltyCost;
    }

    PosLoyaltyDiscount(PosLoyaltyDiscountType type, String id, BigDecimal price) {
        this.type = type;
        this.id = id;
        this.price = price;
    }

    PosLoyaltyDiscount(String id, BigDecimal price) {
        this.type = PosLoyaltyDiscountType.LOYALTY_DISCOUNT;
        this.id = id;
        this.price = price;
    }

    PosLoyaltyDiscount(PosLoyaltyDiscount d) {
        this(d.getType(), d.getId(), d.getPrice(), d.getDescription(), d.getReference(), d.getDiscountCode(), d.getLoyaltyCost());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public PosLoyaltyDiscountType getType() {
        return type;
    }

    public void setType(PosLoyaltyDiscountType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public Short getLoyaltyCost() {
        return loyaltyCost;
    }

    public void setLoyaltyCost(Short loyaltyCost) {
        this.loyaltyCost = loyaltyCost;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
