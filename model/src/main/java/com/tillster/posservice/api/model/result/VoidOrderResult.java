package com.tillster.posservice.api.model.result;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "voidOrderResult")
public class VoidOrderResult {
    private String resultDescription;
    private int statusCode;

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
