package com.tillster.posservice.api.model;

import java.math.BigDecimal;

public class PosLoyaltyPayment {
    public static final String CASH_PAYMENT_TYPE = "CASH";
    public static final String CREDIT_PAYMENT_TYPE = "CREDIT";
    public static final String DEBIT_PAYMENT_TYPE = "DEBIT";
    public static final String LOYALTY_PAYMENT_TYPE = "LOYALTY";
    public static final String GIFT_PAYMENT_TYPE = "GIFT";
    public static final String MOBILE_PAYMENT_TYPE = "MOBILE";

    private String type;
    private BigDecimal amount;
    private String paymentType;
    private String cardType;
    private String maskedCardNumber;
    private String details;

    public PosLoyaltyPayment() {}

    public PosLoyaltyPayment(String type, BigDecimal amount) {
        this.type = type;
        this.amount = amount;
    }

    public PosLoyaltyPayment(PosLoyaltyPayment posLoyaltyPayment) {
        this.type = posLoyaltyPayment.getType();
        this.amount = posLoyaltyPayment.getAmount();
        this.paymentType = posLoyaltyPayment.getPaymentType();
        this.cardType = posLoyaltyPayment.getCardType();
        this.maskedCardNumber = posLoyaltyPayment.getMaskedCardNumber();
        this.details = posLoyaltyPayment.getDetails();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    public void setMaskedCardNumber(String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "Payment{" + type + "/" + paymentType + ",$" + amount + "}";
    }
}
