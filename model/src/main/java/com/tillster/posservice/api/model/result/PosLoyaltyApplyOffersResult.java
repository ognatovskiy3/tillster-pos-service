package com.tillster.posservice.api.model.result;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.tillster.posservice.api.model.PosLoyaltyOfferFailure;
import com.tillster.posservice.api.model.PosLoyaltyOrder;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonRootName(value = "posLoyaltyApplyOffersResult")
public class PosLoyaltyApplyOffersResult {
    private String firstName;
    private String lastName;
    private Long loyaltyPointsBalance;
    private String accountStatus;
    private PosLoyaltyOrder order;
    private List<PosLoyaltyOfferFailure> failures;
    private String resultDescription;
    private int statusCode;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getLoyaltyPointsBalance() {
        return loyaltyPointsBalance;
    }

    public void setLoyaltyPointsBalance(Long loyaltyPointsBalance) {
        this.loyaltyPointsBalance = loyaltyPointsBalance;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public PosLoyaltyOrder getOrder() {
        return order;
    }

    public void setOrder(PosLoyaltyOrder order) {
        this.order = order;
    }

    public List<PosLoyaltyOfferFailure> getFailures() {
        return failures;
    }

    public void setFailures(List<PosLoyaltyOfferFailure> failures) {
        this.failures = failures;
    }

    public String getResultDescription() {
        return resultDescription;
    }

    public void setResultDescription(String resultDescription) {
        this.resultDescription = resultDescription;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
