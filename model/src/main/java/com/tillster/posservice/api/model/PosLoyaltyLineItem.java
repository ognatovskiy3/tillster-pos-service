package com.tillster.posservice.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class PosLoyaltyLineItem {
    private String id;
    private PosLoyaltyDiscountType type;
    private BigDecimal price = BigDecimal.ZERO;
    private int quantity = 1;
    private String category;
    private List<String> categories = new ArrayList<String>();
    private String description;
    private String reference;
    private String discountCode;
    private short loyaltyCost;

    private List<PosLoyaltyLineItem> childItems = new ArrayList<PosLoyaltyLineItem>();

    PosLoyaltyLineItem() {}

    PosLoyaltyLineItem(PosLoyaltyDiscountType type, String id, BigDecimal price) {
        this.type = type;
        this.id = id;
        this.price = price;
    }

    PosLoyaltyLineItem(PosLoyaltyLineItem li) {
        id = li.id;
        type = li.type;
        price = li.price;
        quantity = li.quantity;
        category = li.category;
        categories.addAll(li.categories);
        description = li.description;
        reference = li.reference;
        childItems.addAll(li.getChildItems());
    }

    public void removeChildItems(PosLoyaltyDiscountType type) {
        List<PosLoyaltyLineItem> filtered = childItems.stream().filter(child -> !child.getType().equals(type)).collect(Collectors.toList());
        childItems = filtered;
    }

    public void stripLoyaltyDiscounts() {
        removeChildItems(PosLoyaltyDiscountType.LOYALTY_DISCOUNT);
        childItems.stream().forEach(child -> child.stripLoyaltyDiscounts());
    }

    public BigDecimal calculatePrice(Boolean includeChildren) {
        BigDecimal p = price;
        if (PosLoyaltyDiscountType.POS_DISCOUNT.equals(type) || PosLoyaltyDiscountType.LOYALTY_DISCOUNT.equals(type)) {
            p = p.negate();
        } else if (includeChildren) {
            List<BigDecimal> prices =
                    childItems.stream().map(item -> item.getPrice().multiply(new BigDecimal(item.getQuantity()))).collect(Collectors.toList());
            p = p.add(prices.stream().reduce(BigDecimal.ZERO, BigDecimal::add));
        }
        return p;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PosLoyaltyDiscountType getType() {
        return type;
    }

    public void setType(PosLoyaltyDiscountType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<PosLoyaltyLineItem> getChildItems() {
        return childItems;
    }

    public void setChildItems(List<PosLoyaltyLineItem> childItems) {
        this.childItems = childItems;
    }

    @JsonIgnore
    public List<PosLoyaltyDiscount> getDiscounts() {
        List<PosLoyaltyDiscount> discounts =
                childItems
                        .stream()
                        .filter(
                                child ->
                                        child.getType().equals(PosLoyaltyDiscountType.POS_DISCOUNT)
                                                || child.getType().equals(PosLoyaltyDiscountType.LOYALTY_DISCOUNT))
                        .map(
                                child ->
                                        new PosLoyaltyDiscount(
                                                child.getType(),
                                                child.getId(),
                                                child.getPrice(),
                                                child.getDescription(),
                                                child.getReference(),
                                                child.getDiscountCode(),
                                                child.getLoyaltyCost()))
                        .collect(Collectors.toList());
        return discounts;
    }

    public void addDiscount(PosLoyaltyDiscount discount) {
        PosLoyaltyLineItem item = new PosLoyaltyLineItem(discount.getType(), discount.getId(), discount.getPrice());
        item.setDescription(discount.getDescription());
        item.setReference(discount.getReference());
        item.setDiscountCode(discount.getDiscountCode());
        item.setLoyaltyCost(discount.getLoyaltyCost());

        childItems.add(item);
    }

    public List<String> getCategoriesList() {
        if (categories.isEmpty()) {
            return null;
        }
        return categories;
    }

    @JsonIgnore
    public List<String> getCategories() {
        if (categories.isEmpty() && category != null && StringUtils.isNotEmpty(category)) {
            return Arrays.asList(category);
        }
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public short getLoyaltyCost() {
        return loyaltyCost;
    }

    public void setLoyaltyCost(short loyaltyCost) {
        this.loyaltyCost = loyaltyCost;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return "LineItem{" + id + "," + "'" + description + "'," + quantity + "," + "$" + price + "," + childItems + "}";
    }
}
