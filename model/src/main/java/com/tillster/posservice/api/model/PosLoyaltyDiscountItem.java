
package com.tillster.posservice.api.model;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PosLoyaltyDiscountItem {

    private String description;
    private String displayName;
    private BigDecimal price;
    private String discountCode;
    private String discountSku;
    private PosLoyaltyDiscountItemType type;

    public PosLoyaltyDiscountItem() {}

    public PosLoyaltyDiscountItem(String description, BigDecimal price, String discountCode, String discountSku) {
        this.description = description;
        this.price = price;
        this.discountCode = discountCode;
        this.discountSku = discountSku;
    }

    public String getDescription() {
        return description;
    }

    public PosLoyaltyDiscountItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public PosLoyaltyDiscountItem setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getDiscountSku() {
        return discountSku;
    }

    public void setDiscountSku(String discountSku) {
        this.discountSku = discountSku;
    }

    public PosLoyaltyDiscountItemType getType() {
        return type;
    }

    public void setType(PosLoyaltyDiscountItemType type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
