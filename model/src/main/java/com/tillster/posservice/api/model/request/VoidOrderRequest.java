package com.tillster.posservice.api.model.request;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.tillster.posservice.api.model.PosHeader;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonRootName(value = "voidOrderRequest")
public class VoidOrderRequest {
    private PosHeader header;
    private String shortCode;
    private String loyaltyOrderID;

    public PosHeader getHeader() {
        return header;
    }

    public void setHeader(PosHeader header) {
        this.header = header;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getLoyaltyOrderID() {
        return loyaltyOrderID;
    }

    public void setLoyaltyOrderID(String loyaltyOrderID) {
        this.loyaltyOrderID = loyaltyOrderID;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
