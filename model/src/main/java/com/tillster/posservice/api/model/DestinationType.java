package com.tillster.posservice.api.model;

public enum DestinationType {
    DRIVE_THRU("DRIVE_THRU"),
    TO_GO("TO_GO"),
    DINE_IN("DINE_IN");

    private String value;

    public String getValue() {
        return value;
    }

    private DestinationType(String value) {
        this.value = value;
    }
}
