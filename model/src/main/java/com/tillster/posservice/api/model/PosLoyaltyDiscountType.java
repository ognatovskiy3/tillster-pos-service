package com.tillster.posservice.api.model;

public enum PosLoyaltyDiscountType {
    COMBO("COMBO"),
    PRODUCT("PRODUCT"),
    MODIFIER("MODIFIER"),
    WEIGHT("WEIGHT"),
    POS_DISCOUNT("POS_DISCOUNT"),
    LOYALTY_DISCOUNT("LOYALTY_DISCOUNT"),
    GIFT("GIFT");

    private String type;

    private PosLoyaltyDiscountType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
