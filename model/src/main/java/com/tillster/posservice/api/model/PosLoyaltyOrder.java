package com.tillster.posservice.api.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PosLoyaltyOrder {
    private long posOrderId;
    private long posTransactionTime;
    private BigDecimal orderTotal;
    private BigDecimal orderSubTotal;
    private BigDecimal orderTax;
    private DestinationType destination;
    private String reference;
    private short totalLoyaltyCost;

    private List<PosLoyaltyLineItem> lineItems;
    private List<PosLoyaltyTax> taxes;
    private List<PosLoyaltyDiscount> discounts;
    private List<PosLoyaltyPayment> payments;

    public PosLoyaltyOrder() {}

    public PosLoyaltyOrder(PosLoyaltyOrder from) {
        lineItems = from.getLineItems().stream().map(li -> new PosLoyaltyLineItem(li)).collect(Collectors.toList());
        taxes = from.getTaxes().stream().map(t -> new PosLoyaltyTax(t)).collect(Collectors.toList());
        discounts = from.getDiscounts().stream().map(d -> new PosLoyaltyDiscount(d)).collect(Collectors.toList());
        payments = from.getPayments().stream().map(p -> new PosLoyaltyPayment(p)).collect(Collectors.toList());

        this.posOrderId = from.getPosOrderId();
        this.posTransactionTime = from.getPosTransactionTime();
        this.orderTotal = from.getOrderTotal();
        this.orderSubTotal = from.getOrderSubTotal();
        this.orderTax = from.getOrderTax();
        this.destination = from.getDestination();
        this.reference = from.getReference();
        this.totalLoyaltyCost = from.getTotalLoyaltyCost();
    }

    public PosLoyaltyOrder stripLoyaltyDiscounts() {
        lineItems.stream().forEach(li -> li.stripLoyaltyDiscounts());
        discounts = discounts.stream().filter(d -> d.getType().equals(PosLoyaltyDiscountType.LOYALTY_DISCOUNT)).collect(Collectors.toList());
        return this;
    }

    public List<PosLoyaltyDiscount> allDiscounts() {
        List<PosLoyaltyDiscount> all = new ArrayList<PosLoyaltyDiscount>();
        lineItems.forEach(li -> all.addAll(li.getDiscounts()));
        all.addAll(discounts);
        return all;
    }

    public void addDiscount(PosLoyaltyDiscount discount) {
        discounts.add(discount);
    }

    public List<PosLoyaltyLineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<PosLoyaltyLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public List<PosLoyaltyTax> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<PosLoyaltyTax> taxes) {
        this.taxes = taxes;
    }

    public List<PosLoyaltyDiscount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<PosLoyaltyDiscount> discounts) {
        this.discounts = discounts;
    }

    public long getPosOrderId() {
        return posOrderId;
    }

    public void setPosOrderId(long posOrderId) {
        this.posOrderId = posOrderId;
    }

    public long getPosTransactionTime() {
        return posTransactionTime;
    }

    public void setPosTransactionTime(long posTransactionTime) {
        this.posTransactionTime = posTransactionTime;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public BigDecimal getOrderSubTotal() {
        return orderSubTotal;
    }

    public void setOrderSubTotal(BigDecimal orderSubTotal) {
        this.orderSubTotal = orderSubTotal;
    }

    public BigDecimal getOrderTax() {
        return orderTax;
    }

    public void setOrderTax(BigDecimal orderTax) {
        this.orderTax = orderTax;
    }

    public DestinationType getDestination() {
        return destination;
    }

    public void setDestination(DestinationType destination) {
        this.destination = destination;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Short getTotalLoyaltyCost() {
        return totalLoyaltyCost;
    }

    public void setTotalLoyaltyCost(short totalLoyaltyCost) {
        this.totalLoyaltyCost = totalLoyaltyCost;
    }

    public List<PosLoyaltyPayment> getPayments() {
        return payments;
    }

    public void setPayments(List<PosLoyaltyPayment> payments) {
        this.payments = payments;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
