package com.tillster.posservice.api.model;

public enum PosLoyaltyDiscountItemType {
    LOYALTY("LOYALTY"),
    GENERIC("GENERIC"),
    PERSONAL("PERSONAL");

    private String type;

    private PosLoyaltyDiscountItemType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
