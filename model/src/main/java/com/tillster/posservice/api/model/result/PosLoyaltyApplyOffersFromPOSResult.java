
package com.tillster.posservice.api.model.result;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "posLoyaltyApplyOffersFromPOSResult")
public class PosLoyaltyApplyOffersFromPOSResult extends PosLoyaltyApplyOffersResult {}
