package com.tillster.posservice.api.model;

public enum ReturnStatus {
    UNKNOWN_STATUS(-1),
    SUCCESS(0),
    FAILURE_UNKNOWN(1),
    FAILURE_EXCEPTION(2),
    FAILURE_INVALID(3),
    FAILURE_MAL_ATTEMPT(4),
    FAILURE_INCORRECT_ARGUMENTS(5),
    DUPLICATE_ORDER_ATTEMPT(6),
    TOO_MANY_ATTEMPTS(7),
    UNAUTHORIZED(8),
    FAILURE_CONNECTION(9),
    FAILURE_STATE(10), // Unable to complete operation due to server state
    FAILURE_INPUT(11), // Unable to complete operation with given input
    PARTIAL_SUCCESS(12), // Operation partially completed
    FACEBOOK_CANNOT_RESET_PASSWORD(185),
    PAYMENT_REQUIRED(402),
    NOT_FOUND(404),
    UNSUPPORTED_OPERATION(501),
    GONE(410),
    PRECONDITION_REQUIRED(428),
    FAILURE_CONFLICT(409);

    private int status;

    private ReturnStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
