package com.tillster.platform.pos.svc;

import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersFromPOSRequest;
import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersRequest;
import com.tillster.posservice.api.model.request.PosLoyaltySubmitOrderRequest;
import com.tillster.posservice.api.model.request.VoidOrderRequest;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersFromPOSResult;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersResult;
import com.tillster.posservice.api.model.result.PosLoyaltyGetInfoResult;
import com.tillster.posservice.api.model.result.PosLoyaltySubmitOrderResult;
import com.tillster.posservice.api.model.result.VoidOrderResult;
import com.tillster.posservice.ws.PosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class PosServiceImpl implements PosService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public PosLoyaltyGetInfoResult getInfo() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PosLoyaltyApplyOffersResult applyOffers(PosLoyaltyApplyOffersRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PosLoyaltyApplyOffersFromPOSResult applyOffersFromPOS(PosLoyaltyApplyOffersFromPOSRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PosLoyaltySubmitOrderResult submitOrder(PosLoyaltySubmitOrderRequest request) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public VoidOrderResult voidOrder(VoidOrderRequest request) {
        // TODO Auto-generated method stub
        return null;
    }
}
