package com.tillster.platform.pos.svc;

import com.tillster.posservice.api.model.ReturnStatus;
import com.tillster.posservice.api.model.result.PosLoyaltyGetInfoResult;
import com.tillster.posservice.ws.PosService;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.hibernate3.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.tillster.platform.pos.svc"})
// TODO: Fill in the correct name for the properties file
@PropertySource("classpath:tillster-pos-service-service.properties")
// TODO: Fill in the correct name for the properties file
@PropertySource(value = "file:${catalina.home}/conf/tillster-pos-service-service.properties", ignoreResourceNotFound = true)
public class PosServiceConfiguration {

    @Value("${prop.project.version}")
    private String version;

    @Bean
    public PosLoyaltyGetInfoResult getBuildVersion() {
        PosLoyaltyGetInfoResult result = new PosLoyaltyGetInfoResult();
        result.setApiVersion(version);
        result.setStatusCode(ReturnStatus.SUCCESS.getStatus());
        return result;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySources() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter(@Value("${database.type}") String type, @Value("${database.generateDDL}") Boolean generateDDL) {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(false);
        hibernateJpaVendorAdapter.setGenerateDdl(generateDDL);
        hibernateJpaVendorAdapter.setDatabase(Database.valueOf(type));
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public DataSource dataSource(
            @Value("${database.url}") String url,
            @Value("${database.username}") String username,
            @Value("${database.password}") String password,
            @Value("${database.driver}") String driver) {
        org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("SELECT 1");
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setDataSource(dataSource);
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        // TODO: Replace with valid datamodel entity
        return localContainerEntityManagerFactoryBean;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public HibernateExceptionTranslator exceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    @Bean
    public PosService service() {
        return new PosServiceImpl();
    }
}
