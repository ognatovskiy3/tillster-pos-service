#!/bin/bash
#
# Conditional BitBucket comments depending on Sonar status
# @author Mirko Raner (Tillster, Inc.)
#
# TODO:  Make this file executable.  It loses the executable flag when copied by the archetype
if grep --quiet "big worst" target/sonar/issues-report/issues-report-light.html; then
  MESSAGE="New Sonar issues were introduced: http://jenkins.tillster.com:8080/job/${JOB_NAME}/${BUILD_NUMBER}/artifact/target/sonar/issues-report/issues-report-light.html"
else
  MESSAGE="No Sonar issues were found."
fi
# TODO: Replace mobilem8-components with the correct project name
curl https://api.bitbucket.org/1.0/repositories/tillsterdev/tillster-pos-service/pullrequests/${pullRequestId}/comments \
 --header "Authorization: Basic "$TILLSTER_SONAR_AUTH_TOKEN \
 --data content="$MESSAGE"
