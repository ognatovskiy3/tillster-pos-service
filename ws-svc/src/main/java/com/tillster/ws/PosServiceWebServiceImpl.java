package com.tillster.ws;

import com.tillster.BuildVersion;
import com.tillster.tillster-pos-serviceService;
import com.tillster.ws.api.tillster-pos-serviceWebService;
import com.tillster.ws.model.Model;
import com.tillster.ws.model.ModelId;
import com.tillster.ws.model.exceptions.ModelExistsException;
import com.tillster.ws.model.exceptions.NoSuchModelException;
import com.tillster.model.json.ticket.TicketModel;
import com.tillster.model.json.user.UserType;
import com.tillster.service.user.auth.TillsterAuthContext;

import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class tillster-pos-serviceWebServiceImpl implements tillster-pos-serviceWebService {

    private static final Logger LOGGER = LoggerFactory.getLogger(tillster-pos-serviceWebServiceImpl.class);

    @Inject
    private BuildVersion buildVersion;

    @Inject
    private tillster-pos-serviceService service;

    @Inject
    private TillsterAuthContext tillsterAuthContext;

    //    TODO: Replace with your own methods
    @Override
    public Model createModel(ModelId id, Model model) throws ModelExistsException, NoSuchModelException {
        return service.createModel(id, model);
    }

    //    TODO: Replace with your own methods
    @Override
    public Model getModel(ModelId id) throws NoSuchModelException {
        return service.getModel(id);
    }

    //    TODO: Replace with your own methods
    @Override
    public void deleteModel(ModelId id) {
        //TODO: remove redundant if statment logic
        //this is how you get the ticket model out of the Annotation. the Auth Filter already checks for the
        //admin type but this is just a sample that shows you how to get the ticket/user out of the context
        TicketModel ticketModel = tillsterAuthContext.getTicketModel();
        if( ticketModel.getUserModel().getUserType() == UserType.TILLSTER_ADMIN){
        service.deleteModel(id);
        }
    }

    @Override
    public BuildVersion getVersion() {
        return buildVersion;
    }
}
