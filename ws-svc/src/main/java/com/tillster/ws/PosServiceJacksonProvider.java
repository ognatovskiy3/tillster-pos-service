package com.tillster.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.tillster.platform.common.ws.serialization.StandardObjectMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class tillster-pos-serviceJacksonProvider extends JacksonJaxbJsonProvider {

    private static ObjectMapper objectMapper = new StandardObjectMapper();

    public tillster-pos-serviceJacksonProvider() {
        super(objectMapper, JacksonJaxbJsonProvider.DEFAULT_ANNOTATIONS);
    }
}
