package com.tillster.ws;

import com.tillster.tillster-pos-serviceServiceConfiguration;
import com.tillster.platform.common.ws.PlatformCommonWsConfiguration;
import com.tillster.service.user.auth.AdminTicketIssuer;
import com.tillster.service.user.auth.AuthFilterConfiguration;
import com.tillster.service.user.auth.AuthService;
import com.tillster.service.user.web.TicketWebService;
import com.tillster.service.user.web.UserServiceAuthService;

import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

import javax.ws.rs.client.ClientBuilder;

/**
 * The {@link tillster-pos-serviceWebServiceConfiguration} configures the Location web service.
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@Configuration
// AuthConfiguration is imported here to support the @Authenticated annotation which will look up tickets against the User-service automatically
@Import({tillster-pos-serviceServiceConfiguration.class, AuthFilterConfiguration.class, PlatformCommonWsConfiguration.class })
public class tillster-pos-serviceWebServiceConfiguration {

    @Bean
    public AuthService authService(TicketWebService ticketWebService) {
            return new UserServiceAuthService(ticketWebService);
    }

    @Bean //TODO fix lower case
    public tillster-pos-serviceJacksonProvider tillster-pos-serviceJacksonProvider(){
            return new tillster-pos-serviceJacksonProvider();
    }

    @Bean
    @Lazy
    public TicketWebService ticketWebService(@Value("${auth.url}") String url,tillster-pos-serviceJacksonProvider provider) {
            return WebResourceFactory.newResource(TicketWebService.class, ClientBuilder.newClient().register(provider).target(url));
    }

    @Bean
    @Lazy
    public AdminTicketIssuer adminTicketIssuer(
    @Value("${auth.adminUser}") String adminUser,
    @Value("${auth.adminPass}") String adminPass,
    @Value("${auth.adminTenant}") String tenant) {
            return new AdminTicketIssuer(adminUser, adminPass, tenant);
    }
}
