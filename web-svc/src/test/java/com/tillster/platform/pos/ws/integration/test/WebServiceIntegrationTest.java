package com.tillster.platform.pos.ws.integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tillster.platform.pos.ws.QuickTest;
import com.tillster.platform.pos.ws.SlowTest;
import com.tillster.test.integration.ws.framework.AbstractIntegrationTest;
import com.tillster.test.integration.ws.framework.MockServlet;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.startup.Tomcat;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link WebServiceIntegrationTest} is an integration test that verifies the basic
 * functionality of the {@link tillster-pos-serviceWebService}. It installs the WAR file on an embedded
 * {@link Tomcat} server and then sends HTTP requests to that server.
 *
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@Category({QuickTest.class, SlowTest.class})
@Ignore
public class WebServiceIntegrationTest extends AbstractIntegrationTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceIntegrationTest.class);

    private static final String MOCK_AUTH = "user-service-web";
    private static final String VALID_TICKET = "12345678-9ABC-DEF0-1234-567890ABCDEF";
    private static final String INVALID_TICKET = "00000000-0000-0000-0000-000000000000";
    private static final Pattern ticketPattern = Pattern.compile("\\{\"ticketId\":\"([A-Z0-9-]+)\"\\}");

    private static final String MODEL_NAME = "thailand";
    private static final String MODEL = "{\"name\":\"" + MODEL_NAME + "\"}";

    private static Map<String, String> ticketToUser = new HashMap<>();

    static {
        ticketToUser.put(VALID_TICKET, "user1");
    }

    // TODO: change name
    private static final String SERVICE_NAME = "tillster-pos-service-ws";

    private static boolean mockExternalServices = true;
    private static int port = 8181;
    private static String baseUrl;
    private static Tomcat server;
    private static JsonParser parser = new JsonParser();
    private CloseableHttpClient httpClient;

    private static MockServlet setMockServlet() {
        return new MockServlet(getAuthServlet(), "external.dependency.url", MOCK_AUTH);
    }

    public WebServiceIntegrationTest() {
        super(SERVICE_NAME, setMockServlet());
    }

    private static InputStream resourceStream(String path) {
        ClassLoader loader = WebServiceIntegrationTest.class.getClassLoader();
        return loader.getResourceAsStream(path);
    }

    @After
    public void cleanUp() throws Exception {
        assertEquals(204, url("/rest/changeme/models/" + MODEL_NAME).ticket(VALID_TICKET).delete());
    }

    @Override
    protected void setProperties() {
        try {
            InputStream testProperties = resourceStream("tillster-pos-service-service-test.properties");
            Properties properties = new Properties();
            properties.load(testProperties);
            properties.forEach((key, value) -> System.setProperty((String) key, (String) value));
            System.setProperty("auth.url", "http://localhost:" + port + "/" + MOCK_AUTH);
        } catch (IOException ioe) {
            LOGGER.debug("there was an error creating the props");
        }
    }

    @Test
    public void version() throws Exception {
        //    TODO: Correct URL
        JsonElement result = url("/rest/changeme/version").get();
        assertNotNull(result);
        assertEquals("test123", result.getAsJsonObject().get("version").getAsString());
    }

    //    TODO: Remove this test
    @Test
    public void createModel() throws Exception {
        JsonElement result = url("/rest/changeme/models/" + MODEL_NAME).body(MODEL).ticket(VALID_TICKET).post();
        JsonElement expected = parser.parse(MODEL); // result should be equal to input document
        assertEquals(expected, result);
    }

    //    TODO: Remove this test
    @Test
    public void getModel() throws Exception {
        JsonElement result = url("/rest/changeme/models/" + MODEL_NAME).body(MODEL).ticket(VALID_TICKET).post();
        JsonElement expected = parser.parse(MODEL); // result should be equal to input document
        assertEquals(expected, result);

        result = url("/rest/changeme/models/" + MODEL_NAME).get();
        assertEquals(expected, result);
    }

    //    TODO: Remove this test
    @Test
    public void deleteModel() throws Exception {
        url("/rest/changeme/models/" + MODEL_NAME).body(MODEL).ticket(VALID_TICKET).post();
        assertEquals(204, url("/rest/changeme/models/" + MODEL_NAME).ticket(VALID_TICKET).delete());
    }

    private static HttpServlet getAuthServlet() {
        return new HttpServlet() {

            @Override
            protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                if (request.getRequestURI().startsWith("/" + MOCK_AUTH)) {
                    int length = request.getContentLength();
                    byte[] content = new byte[length];
                    DataInputStream stream = new DataInputStream(request.getInputStream());
                    stream.readFully(content);
                    String messageBody = new String(content);
                    Matcher matcher = ticketPattern.matcher(messageBody);
                    if (matcher.matches() && ticketToUser.containsKey(matcher.group(1))) {
                        String user = ticketToUser.get(matcher.group(1));
                        response.setHeader("Content-Type", "application/json");
                        response.getWriter()
                                .println(
                                        "{"
                                                + "\"id\":\"d392fddf-5809-42f3-b81d-85af97b6da34\","
                                                + "\"userId\":\""
                                                + user
                                                + "\","
                                                + "\"expiryTime\":1451522650456,"
                                                + "\"tenant\":\"br\","
                                                + "\"userType\":\"TILLSTER_ADMIN\","
                                                + "\"userModel\":{"
                                                + "\"userType\":\"TILLSTER_ADMIN\","
                                                + "\"email\":\"someEmail\"}"
                                                + "}");
                        response.setStatus(200);
                    } else {
                        response.setStatus(404);
                    }
                } else {
                    response.setStatus(500);
                }
            }
        };
    }
}
