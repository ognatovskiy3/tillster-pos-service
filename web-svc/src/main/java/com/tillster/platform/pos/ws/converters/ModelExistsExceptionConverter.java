package com.tillster.platform.pos.ws.converters;
//package com.tillster.ws.converters;
//
//import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
//import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;
//
//import com.tillster.ws.model.exceptions.ModelExistsException;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.ext.ExceptionMapper;
//import javax.ws.rs.ext.Provider;
//
///**
// * Created by robert on 8/25/16.
// */
//@Provider
//public class ModelExistsExceptionConverter implements ExceptionMapper<ModelExistsException> {
//
//    static class Error {
//
//        private ModelExistsException modelExistsException;
//
//        public ModelExistsException getModelExistsException() {
//            return modelExistsException;
//        }
//
//        public void setModelExistsException(ModelExistsException exception) {
//            this.modelExistsException = exception;
//        }
//    }
//
//    @Override
//    public Response toResponse(ModelExistsException exception) {
//
//        Error entity = new Error();
//        entity.setModelExistsException(exception);
//        return Response.status(BAD_REQUEST).entity(entity).build();
//    }
//}
