package com.tillster.platform.pos.ws.converters;
//package com.tillster.ws.converters;
//
//import static javax.ws.rs.core.Response.Status.NOT_FOUND;
//
//import com.tillster.ws.model.exceptions.NoSuchModelException;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.ext.ExceptionMapper;
//import javax.ws.rs.ext.Provider;
//
///**
// * Created by robert on 8/25/16.
// */
//@Provider
//public class NoSuchModelExceptionConverter implements ExceptionMapper<NoSuchModelException> {
//
//    static class Error {
//
//        private NoSuchModelException noSuchModelException;
//
//        public NoSuchModelException getNoSuchModelException() {
//            return noSuchModelException;
//        }
//
//        public void setNoSuchModelException(NoSuchModelException noSuchModelException) {
//            this.noSuchModelException = noSuchModelException;
//        }
//    }
//
//    @Override
//    public Response toResponse(NoSuchModelException noSuchModelException) {
//
//        Error entity = new Error();
//        entity.setNoSuchModelException(noSuchModelException);
//        return Response.status(NOT_FOUND).entity(entity).build();
//    }
//}
