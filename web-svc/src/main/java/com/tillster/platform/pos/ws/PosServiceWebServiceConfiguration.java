package com.tillster.platform.pos.ws;

import com.tillster.platform.common.ws.PlatformCommonWsConfiguration;
import com.tillster.platform.pos.svc.PosServiceConfiguration;
import com.tillster.service.user.auth.AdminTicketIssuer;
import com.tillster.service.user.auth.AuthFilterConfiguration;
import com.tillster.service.user.auth.AuthService;
import com.tillster.service.user.web.TicketWebService;
import com.tillster.service.user.web.UserServiceAuthService;
import javax.ws.rs.client.ClientBuilder;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import({PosServiceConfiguration.class, AuthFilterConfiguration.class, PlatformCommonWsConfiguration.class})
public class PosServiceWebServiceConfiguration {

    @Bean
    public AuthService authService(TicketWebService ticketWebService) {
        return new UserServiceAuthService(ticketWebService);
    }

    @Bean
    public PosServiceJacksonProvider PosServiceJacksonProvider() {
        return new PosServiceJacksonProvider();
    }

    @Bean
    @Lazy
    public TicketWebService ticketWebService(@Value("${auth.url}") String url, PosServiceJacksonProvider provider) {
        return WebResourceFactory.newResource(TicketWebService.class, ClientBuilder.newClient().register(provider).target(url));
    }

    @Bean
    @Lazy
    public AdminTicketIssuer adminTicketIssuer(
            @Value("${auth.adminUser}") String adminUser, @Value("${auth.adminPass}") String adminPass, @Value("${auth.adminTenant}") String tenant) {
        return new AdminTicketIssuer(adminUser, adminPass, tenant);
    }
}
