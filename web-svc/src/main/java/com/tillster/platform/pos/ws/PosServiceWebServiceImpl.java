package com.tillster.platform.pos.ws;

import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersFromPOSRequest;
import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersRequest;
import com.tillster.posservice.api.model.request.PosLoyaltySubmitOrderRequest;
import com.tillster.posservice.api.model.request.VoidOrderRequest;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersFromPOSResult;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersResult;
import com.tillster.posservice.api.model.result.PosLoyaltyGetInfoResult;
import com.tillster.posservice.api.model.result.PosLoyaltySubmitOrderResult;
import com.tillster.posservice.api.model.result.VoidOrderResult;
import com.tillster.posservice.ws.PosService;
import com.tillster.posservice.ws.PosWebService;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PosServiceWebServiceImpl implements PosWebService {

    @Inject
    private PosService service;

    @Override
    public PosLoyaltyGetInfoResult getInfo() {
        return service.getInfo();
    }

    @Override
    public PosLoyaltyApplyOffersResult applyOffers(PosLoyaltyApplyOffersRequest request) {
        return service.applyOffers(request);
    }

    @Override
    public PosLoyaltyApplyOffersFromPOSResult applyOffersFromPOS(PosLoyaltyApplyOffersFromPOSRequest request) {
        return service.applyOffersFromPOS(request);
    }

    @Override
    public PosLoyaltySubmitOrderResult submitOrder(PosLoyaltySubmitOrderRequest request) {
        return service.submitOrder(request);
    }

    @Override
    public VoidOrderResult voidOrder(VoidOrderRequest request) {
        return service.voidOrder(request);
    }
}
