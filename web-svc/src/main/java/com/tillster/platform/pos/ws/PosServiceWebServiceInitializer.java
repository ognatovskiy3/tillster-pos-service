package com.tillster.platform.pos.ws;

import static org.springframework.web.context.ContextLoader.CONFIG_LOCATION_PARAM;
import static org.springframework.web.context.ContextLoader.CONTEXT_CLASS_PARAM;

import com.tillster.service.user.auth.AuthFeature;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

@Order(Ordered.HIGHEST_PRECEDENCE)
public class PosServiceWebServiceInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext context) throws ServletException {
        context.setInitParameter(CONTEXT_CLASS_PARAM, AnnotationConfigWebApplicationContext.class.getName());
        context.setInitParameter(CONFIG_LOCATION_PARAM, PosServiceWebServiceConfiguration.class.getName());
        context.addListener(RequestContextListener.class);
        context.addListener(ContextLoaderListener.class);
        ServletRegistration.Dynamic registration = context.addServlet("jersey-servlet", "org.glassfish.jersey.servlet.ServletContainer");
        registration.setInitParameter(
                "jersey.config.server.provider.classnames",
                PosServiceWebServiceImpl.class.getName()
                        + ", org.glassfish.jersey.filter.LoggingFilter ,"
                        + PosServiceJacksonProvider.class.getName()
                        + ","
                        + AuthFeature.class);
        // TODO: Add provider packages and uncomment
        //        registration.setInitParameter("jersey.config.server.provider.packages", "com.tillster.ws.converters);
        registration.setInitParameter("jersey.config.server.tracing.type", "OFF");
        registration.setInitParameter("jersey.config.server.tracing.threshold", "TRACE");
        registration.setLoadOnStartup(1);
        registration.addMapping("/rest/*");
    }
}
