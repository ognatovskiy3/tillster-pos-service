package com.tillster.posservice.ws;

import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersFromPOSRequest;
import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersRequest;
import com.tillster.posservice.api.model.request.PosLoyaltySubmitOrderRequest;
import com.tillster.posservice.api.model.request.VoidOrderRequest;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersFromPOSResult;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersResult;
import com.tillster.posservice.api.model.result.PosLoyaltyGetInfoResult;
import com.tillster.posservice.api.model.result.PosLoyaltySubmitOrderResult;
import com.tillster.posservice.api.model.result.VoidOrderResult;

public interface PosService {
    PosLoyaltyGetInfoResult getInfo();

    PosLoyaltyApplyOffersResult applyOffers(PosLoyaltyApplyOffersRequest request);

    PosLoyaltyApplyOffersFromPOSResult applyOffersFromPOS(PosLoyaltyApplyOffersFromPOSRequest request);

    PosLoyaltySubmitOrderResult submitOrder(PosLoyaltySubmitOrderRequest request);

    VoidOrderResult voidOrder(VoidOrderRequest request);
}
