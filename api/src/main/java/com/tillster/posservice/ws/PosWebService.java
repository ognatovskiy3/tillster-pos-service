package com.tillster.posservice.ws;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersFromPOSRequest;
import com.tillster.posservice.api.model.request.PosLoyaltyApplyOffersRequest;
import com.tillster.posservice.api.model.request.PosLoyaltySubmitOrderRequest;
import com.tillster.posservice.api.model.request.VoidOrderRequest;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersFromPOSResult;
import com.tillster.posservice.api.model.result.PosLoyaltyApplyOffersResult;
import com.tillster.posservice.api.model.result.PosLoyaltyGetInfoResult;
import com.tillster.posservice.api.model.result.PosLoyaltySubmitOrderResult;
import com.tillster.posservice.api.model.result.VoidOrderResult;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface PosWebService extends PosService {

    @GET
    @Path("/getInfo")
    @Override
    PosLoyaltyGetInfoResult getInfo();

    @POST
    @Path("/applyOffers")
    @Override
    PosLoyaltyApplyOffersResult applyOffers(PosLoyaltyApplyOffersRequest request);

    @POST
    @Path("/applyOffersFromPOS")
    @Override
    PosLoyaltyApplyOffersFromPOSResult applyOffersFromPOS(PosLoyaltyApplyOffersFromPOSRequest request);

    @POST
    @Path("/submitOrder")
    @Override
    PosLoyaltySubmitOrderResult submitOrder(PosLoyaltySubmitOrderRequest request);

    @POST
    @Path("/voidOrder")
    @Override
    VoidOrderResult voidOrder(VoidOrderRequest request);
}
